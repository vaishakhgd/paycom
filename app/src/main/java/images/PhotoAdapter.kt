package images



import com.github.chrisbanes.photoview.PhotoView
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.viven.imagezoom.ImageZoomHelper

class YO(private val myDataset: List<String>) :
    RecyclerView.Adapter<YO.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val imageVIew: PhotoView) : RecyclerView.ViewHolder(imageVIew)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): YO.MyViewHolder {
        // create a new view

        val imgView = PhotoView(parent.context)
        // set the view's size, margins, paddings and layout parameters



        imgView.isZoomable = true


        return MyViewHolder(imgView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        // holder.textView.text = myDataset[position]
        var options =  BitmapFactory.Options();
        options.inSampleSize = 1

        var resultBitmap = BitmapFactory.decodeFile( myDataset[position],options);

        holder.imageVIew.isZoomable = true


        holder.imageVIew.setImageBitmap(resultBitmap)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}