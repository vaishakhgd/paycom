package chat

import android.util.Log
import android.widget.*
import com.example.paycom.ChatWindowActivity
import com.example.paycom.R
import com.sendbird.android.BaseChannel
import com.sendbird.android.BaseMessage
import com.sendbird.android.OpenChannel

public class ChatAdapter (channel: OpenChannel,chatWindowActivity: ChatWindowActivity)
     {
         private val VIEW_TYPE_MESSAGE_SENT = 1
         private val VIEW_TYPE_MESSAGE_RECEIVED = 2
         private val chatWindowActivityOfAdapter :  ChatWindowActivity

         private var mMessageList = mutableListOf<BaseMessage>()
         private  final val mChannel: OpenChannel

         init {
             mMessageList= mutableListOf<BaseMessage>()
             mChannel = channel
             refresh()
             chatWindowActivityOfAdapter = chatWindowActivity
         }
         fun returnMessageList() : List<BaseMessage>{
         return mMessageList
     }

         fun addAllMessages(mMessageList: kotlin.collections.List<BaseMessage>){
              var l3 =  chatWindowActivityOfAdapter.findViewById<LinearLayout>(R.id.linear)
             var b= Button(chatWindowActivityOfAdapter)
             b.setText("Send")
             var editext = EditText(chatWindowActivityOfAdapter)
             l3.addView(editext)
             l3.addView(b)
             for(i in mMessageList){
                 var textView = TextView(chatWindowActivityOfAdapter)
                 textView.setText(i.toString())
                 l3.addView(textView)
             }
         }


         fun sendMessageToOther(s: String){
             mChannel.sendUserMessage(s,
                 BaseChannel.SendUserMessageHandler { userMessage, e ->
                     if (e != null) {
                         e.printStackTrace()
                         return@SendUserMessageHandler
                     }

                     mMessageList.add(0, userMessage)
                     chatWindowActivityOfAdapter.mMessageList =mMessageList
                     chatWindowActivityOfAdapter.addIndividualMessage(userMessage)

                 })
         }



          fun refresh() {
             mChannel.getPreviousMessagesByTimestamp(java.lang.Long.MAX_VALUE, true, 100, true,
                 BaseChannel.MessageTypeFilter.USER, null,
                 BaseChannel.GetMessagesHandler { list1, e ->
                     if (e != null) {
                         e.printStackTrace()
                         return@GetMessagesHandler
                     }
                     mMessageList = list1
                    // addAllMessages(list1)
                     chatWindowActivityOfAdapter.mMessageList =mMessageList
                     chatWindowActivityOfAdapter.addMessages()
                     for(l in list1){
                         Log.i("yo",l.toString())
                     }
                 })
          }

          fun loadPreviousMessages() {
              val p = (mMessageList.size)  - 1
             val lastTimestamp = mMessageList.get(p).createdAt
             mChannel.getPreviousMessagesByTimestamp(lastTimestamp, false, 100, true,
                 BaseChannel.MessageTypeFilter.USER, null,
                 BaseChannel.GetMessagesHandler { list, e ->
                     if (e != null) {
                         e.printStackTrace()
                         return@GetMessagesHandler
                     }
                     mMessageList.addAll(list)

                     for(l in list){
                         //Log.i("tag",l.toString())

                     }


                 })
         }

         internal fun sendMessage(message: String) {
             mChannel.sendUserMessage(message,
                 BaseChannel.SendUserMessageHandler { userMessage, e ->
                     if (e != null) {
                         e.printStackTrace()
                         return@SendUserMessageHandler
                     }

                     mMessageList.add(0, userMessage)

                 })
         }





}