package calendar

import ApiEndpoints.Event
import Database.EventEntity
import android.content.ContentUris
import android.content.Intent
import android.net.Uri
import android.provider.CalendarContract
import android.util.Log
import com.example.paycom.DispalyEventDetailsActivity
import com.google.gson.Gson

import java.util.*
import android.widget.Toast
import android.provider.CalendarContract.Events
import com.example.paycom.ui.login.encryption.Hasher


class WriteToCalendar(ctx2 : DispalyEventDetailsActivity) {

    val ctx : DispalyEventDetailsActivity

    init {
        ctx = ctx2
    }

    public fun modifyCalendar(){
         val DEBUG_TAG: String = "MyActivity"

    }

    public  fun deleteEvent(eventId :Long){

        val eventID :Long = 201
        var deleteUri: Uri? = null
        deleteUri = ContentUris.withAppendedId(
            CalendarContract.Events.CONTENT_URI,
            6.toLong()
        )




        val rows = ctx.contentResolver.delete(deleteUri, null, null)
        Toast.makeText(ctx, "Event deleted " +rows.toString(), Toast.LENGTH_LONG).show()


        val uri = ContentUris.withAppendedId(Events.CONTENT_URI, 6.toLong())
        val intent = Intent(Intent.ACTION_EDIT) .setData(uri)
            .putExtra(Events.TITLE, "My New Title");


        ctx.startActivity(intent)
    }


    public fun insertToCalendar(eventGlobal : EventEntity){

        var gson = Gson()
        val event = gson.fromJson<Event>(eventGlobal.event,Event::class.java)

       // deleteEvent(eventGlobal)
        for(i in  0..(event.sessionList.size-1)) {


            if(event.isGoing.get(i)    ) {

                Log.i("hi", "event.isGoing.get(i)event.isGoing.get(i)event.isGoing.get(i)event.isGoing.get(i)event.isGoing.get(i) "+event.isGoing.get(i))
                var dateArr = event.Date.split("-")



                val calID: Long = 1
                val startMillis: Long = Calendar.getInstance().run {
                    set(dateArr[2].toInt(), dateArr[1].toInt(), dateArr[0].toInt(),event.sessionList.get(i).split("-")[0].toInt(), 0)
                    timeInMillis
                }
                val endMillis: Long = Calendar.getInstance().run {
                    set(dateArr[2].toInt(), dateArr[1].toInt(), dateArr[0].toInt(), event.sessionList.get(i).split("-")[1].toInt(), 0)
                    timeInMillis
                }




                val calIntent = Intent(Intent.ACTION_INSERT)
                calIntent.type = "vnd.android.cursor.item/event"
                calIntent.putExtra(CalendarContract.Events.TITLE,event.eventno )
                calIntent.putExtra(CalendarContract.Events.DESCRIPTION, event.description)
                calIntent.putExtra(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID())
                val eid : Long = startMillis+endMillis+event.eventId.toLong()
                val hash =Hasher()

                val e  = event.eventno + i.toString()

                val eventIdForCalendar= hash.hash(e)

                calIntent.putExtra(CalendarContract.Events._ID, eid.toString())

                var emails = ""

                for(i in 0..(event.guestlist.size-1) ){
                    calIntent.putExtra(CalendarContract.Attendees.ATTENDEE_NAME,event.guestlist.get(i))
                    calIntent.putExtra(CalendarContract.Attendees.ATTENDEE_EMAIL,event.guestlist.get(i) +"@gmail.com")
                    calIntent.putExtra(CalendarContract.Attendees.EVENT_ID,eventIdForCalendar)
                   // calIntent.putExtra(CalendarContract.Attendees.
                    emails+=event.guestlist.get(i) +"@gmail.com,"

                    calIntent.putExtra(CalendarContract.Attendees.GUESTS_CAN_SEE_GUESTS,true)




                }
                calIntent.putExtra(Intent.EXTRA_EMAIL, emails);

                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,  endMillis)
                ctx.startActivity(calIntent)
            }

        }
    }

    public fun findIfEventPresentInCalendar(eventGlobal : EventEntity){

        insertToCalendar(eventGlobal)
    }
}
