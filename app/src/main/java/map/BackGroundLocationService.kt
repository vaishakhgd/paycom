package map

import Database.SharedPreferences.SharedPreferenceManagerForLocationDetails
import Database.SharedPreferences.SharedPreferenceManagerForUserDetails
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.example.paycom.MainActivity
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import map.StoreDataToParse.ParseLocationData
import java.util.*
import java.util.concurrent.TimeUnit


class BackGroundLocationService : Service() ,LocationListener {

    var isServiceStopped = false

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    lateinit var locationCallback: LocationCallback

    val ctx2 : MainActivity

    init {
        ctx2 = MainActivity()
    }

    override fun onLocationChanged(p0: Location?) {
        Toast.makeText(this.applicationContext,"LONGITUDE is "+ p0!!.longitude,Toast.LENGTH_LONG).show()
    }


   override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "Service started by user. LOOO TAGARU", Toast.LENGTH_LONG).show()
        Log.i("Yo","AAAAAAAAAQQEERQRFGGGHHHHHHHHHHHHHHHHHHHHHHHHH")
        var mTimer = Timer() //recreate new
        isServiceStopped = false

        val notify =  300000
        mTimer.scheduleAtFixedRate(TimeDisplay(this), 0, 60*1000*3)
        onLocationChangeRegister()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "Service destroyed by user. LOOOOO DOLLY", Toast.LENGTH_LONG).show()
        isServiceStopped = true

       // fusedLocationProviderClient.removeLocationUpdates(locationCallback)

    }

    fun saveDetailsToSharedPref(latLng: LatLng){

        val sharedPreferenceManagerForLocationDetails = SharedPreferenceManagerForLocationDetails(this.applicationContext)

        val userDetails = SharedPreferenceManagerForUserDetails(this)

        val locationData = LocationData(latitude = latLng.latitude,
            longitude = latLng.longitude,
            time = System.currentTimeMillis(),email = userDetails.getSavedUserDetails().email)

        sharedPreferenceManagerForLocationDetails.storeLocationDetails(locationData)
    }


    fun onLocationChangeRegister()
    {

        val locationRequest = LocationRequest.create()?.apply {
            interval = TimeUnit.SECONDS.toMillis(30)
            fastestInterval = TimeUnit.SECONDS.toMillis(30)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        }



        val contxt = this

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    // Update UI with location data
                    // ...
                    Log.d("Location is ", "LOCATIION " + location.latitude)

                    val latLng = LatLng(location!!.latitude,location!!.longitude)

                    Log.d("Location is ", "LOCATIION " + location.latitude)

                    saveDetailsToSharedPref(latLng)


                    if(isServiceStopped ==false) {
                        Toast.makeText(
                            contxt.applicationContext,
                            "isServiceStopped == false",
                            Toast.LENGTH_LONG
                        ).show()
                        val parseLocationData = ParseLocationData(contxt)
                        parseLocationData.deleteExisting()
                        val userDetails = SharedPreferenceManagerForUserDetails(contxt)
                    }





                    Toast.makeText(this@BackGroundLocationService.applicationContext,        "yo: "
                            +latLng.latitude.toString()
                            +" :: "
                            +latLng.longitude.toString(),    Toast.LENGTH_LONG).show()

                    Log.i("YOPPPPPPOO ", "    yo: "
                            +latLng.latitude.toString()
                            +" :: "
                            +latLng.longitude.toString())
                }
            }
        }

        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest,
            locationCallback,
            Looper.getMainLooper())
    }


    override fun onCreate() {
        super.onCreate()
        Log.d("Yo", "onCreate()")
        isServiceStopped = false
        // TODO: Step 1.2, Review the FusedLocationProviderClient.
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
    }



    internal class TimeDisplay(ctx : Context) : TimerTask() {
        var mHandler = Handler()

        val contxt = ctx

        override fun run() {
            // run on another thread
           mHandler.post( Runnable {

              // onLocationChangeRegister()
           } )
        }
    }
}