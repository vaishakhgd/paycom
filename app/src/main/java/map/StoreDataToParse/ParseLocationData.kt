package map.StoreDataToParse

import Database.EventDao
import Database.SharedPreferences.SharedPreferenceManagerForLocationDetails
import android.content.Context
import android.widget.Toast
import com.parse.ParseObject
import com.parse.ParseQuery
import map.LocationData

class ParseLocationData(ctx : Context) {

    val ctx2 : Context

    init {
        ctx2 = ctx
    }

    public fun deleteExisting(){
        val query = ParseQuery.getQuery<ParseObject>("LocationData")
        val sharedPreferenceManagerForLocationDetails = SharedPreferenceManagerForLocationDetails(ctx2)

        if(sharedPreferenceManagerForLocationDetails.isLocationStored()) {
            val locationDetails =
                sharedPreferenceManagerForLocationDetails.getSavedLocationDetails()

            query.whereContains("email", locationDetails.email)

            query.findInBackground { objects, e ->
                if (e == null) {
                    for (parseObject in objects) {
                        parseObject.delete()//calling blocking system call because, only upon successful deletion upload must happen
                    }
                    uponDeletionUpload(locationDetails)
                } else {
                }
            }
        }


    }

    public  fun uponDeletionUpload(locationData : LocationData){

        Toast.makeText(ctx2.applicationContext, "Uploading ",Toast.LENGTH_LONG).show()

        val uploadParseObject = ParseObject("LocationData")
        uploadParseObject.put("email", locationData.email);
        uploadParseObject.put("latitude",locationData.latitude);
        uploadParseObject.put("longitude", locationData.longitude);
        uploadParseObject.put("time", locationData.time);
        uploadParseObject.saveInBackground()

    }
}