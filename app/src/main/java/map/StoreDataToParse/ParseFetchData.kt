package map.StoreDataToParse

import Database.SharedPreferences.SharedPreferenceManagerForLocationDetails
import android.content.Context
import com.example.paycom.MapEventLoadActivity
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.parse.ParseObject
import com.parse.ParseQuery
import map.LocationData

class ParseFetchData(ctx : Context, mapEventLoadActivity: MapEventLoadActivity) {

    val ctx2 : Context

    var mapEventLoadActivity2 : MapEventLoadActivity

    init{
        ctx2 = ctx

        mapEventLoadActivity2 = mapEventLoadActivity
    }

    public fun getMarker(){
        val query = ParseQuery.getQuery<ParseObject>("LocationData")

        var mutableList  = mutableListOf<LocationData>()


        query.findInBackground { objects, e ->
                if (e == null) {
                    for (parseObject in objects) {
                        val latitude= parseObject.getDouble("latitude")
                        val longitude = parseObject.getDouble("longitude")
                        val time = parseObject.getLong("time")
                        val email = parseObject.getString("email")
                        val locationData = LocationData(latitude = latitude,longitude = longitude,
                        time = time,email = email!!)

                        mutableList.add(locationData)
                    }

                    saveQueryDetails(mutableList)



            }
        }


    }

    public  fun saveQueryDetails(mutableList : MutableList<LocationData>){

        mapEventLoadActivity2.googleMap!!.clear()

        if(mutableList.isNotEmpty()){
            for(location in mutableList){

                val marker = MarkerOptions()

                val latlng = LatLng(location.latitude,location.longitude)

                marker.position(latlng).title(location.email)

                mapEventLoadActivity2.googleMap!!.addMarker(marker)

            }
        }

    }
}