package com.example.paycom

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity

import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import chat.ChatAdapter
import com.sendbird.android.*


class ChatWindowActivity : AppCompatActivity() {

    private var mChannelUrl =  "channelurl"
    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_CHAT"
    private var mChatAdapterGlobal: ChatAdapter? = null
    private var mRecyclerView: RecyclerView? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var mSendButton: Button? = null
    private var mMessageEditText: EditText? = null

    var isChatAdapterInstantiated = false

    public var mMessageList = mutableListOf<BaseMessage>()


    fun addMessages(){
        var l3 =  findViewById(R.id.linear) as (LinearLayout)

        for(i in mMessageList){
            var textView = TextView(this)

            var x =   i as (UserMessage)
            textView.setText( x.message.toString() )
            Log.i("cool",i.toString())
            l3.addView(textView)
        }
    }

    fun addIndividualMessage( userMessage: UserMessage){
        var l3 =  findViewById(R.id.linear) as (LinearLayout)
        var textView = TextView(this)
        textView.setText( userMessage.message.toString() )
        l3.addView(textView)
    }




    /**
     * Updates the user's nickname.
     * @param userNickname  The new nickname of the user.
     *
     *
     */



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_chat_window)
        var l3 =  findViewById(R.id.linear) as (LinearLayout)
        val b2= Button(this)
        b2.setText("Send")





        var editext = findViewById(R.id.SendMsg) as (EditText)


        var b = findViewById<Button>(R.id.SendButton)




        // UNCOMMENT BELOW THING IN ORIGINAL CODE ONLY IF ChannelUrl under event name is already created in SnapBird
       // mChannelUrl = intent.getStringExtra("channelUrlIdeallyMustBeEqualToEventId")
        //

        var mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.reverseLayout = true


        OpenChannel.getChannel(mChannelUrl,
            OpenChannel.OpenChannelGetHandler { openChannel, e ->
                if (e != null) {
                    e.printStackTrace()
                    return@OpenChannelGetHandler
                }

                openChannel.enter(OpenChannel.OpenChannelEnterHandler { e ->
                    if (e != null) {
                        e.printStackTrace()
                        return@OpenChannelEnterHandler
                    }

                    mChatAdapterGlobal = ChatAdapter(openChannel, this)
                    isChatAdapterInstantiated =true

                    mChatAdapterGlobal?.refresh()
                })
            })

        b.setOnClickListener {
            var editmsg = editext.text.toString()

            if(editmsg.length>=1 && isChatAdapterInstantiated ){

                mChatAdapterGlobal?.sendMessageToOther(editmsg)
                Log.i("mmm","MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
              //  mChatAdapterGlobal = ChatAdapter(openChannel, this)
            }
        }


    }

   override protected fun onResume() {
        super.onResume()

        // Receives messages from SendBird servers
        SendBird.addChannelHandler(CHANNEL_HANDLER_ID, object : SendBird.ChannelHandler() {
            override fun onMessageReceived(baseChannel: BaseChannel, baseMessage: BaseMessage) {
                if (baseChannel.url == mChannelUrl && baseMessage is UserMessage) {

                    Log.i("mmm","Mesage receiveddddddddddddddddddddddd isssssssssssssssss" + baseMessage.toString())

                    addIndividualMessage(baseMessage)

                }
            }
        })
    }

   override public fun onPause() {
        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID)

        super.onPause()
    }







}
