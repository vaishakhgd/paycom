package com.example.paycom

import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ScaleGestureDetector
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chrisbanes.photoview.PhotoView
import com.viven.imagezoom.ImageZoomHelper
import images.YO
import java.io.File
import java.lang.Exception

class EventPhotoGalleryActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    var imageView: ImageView? = null


    fun readImages(){
        var options = BitmapFactory.Options();

        imageView = ImageView(this)
        options.inSampleSize = 4

        try {
            var file = File("/storage/emulated/0/DCIM/Screenshots/")

            file = File("/storage/emulated/0/DCIM/Camera/vaishakh/vaishakh/images/")

            var p = file.listFiles()

            var imageViewList = mutableListOf<String>()

            for (files in p) {
                imageViewList.add(files.absolutePath)
            }
            var resultBitmap =
                BitmapFactory.decodeFile("/storage/emulated/0/DCIM/Screenshots/wow.jpg", options);
            imageView!!.setImageBitmap(resultBitmap)

            val imageZoomManager = ImageZoomHelper(this)

            viewManager = LinearLayoutManager(this)
            viewAdapter = YO(imageViewList)

            val photoView = PhotoView(this)
            photoView.setImageBitmap(resultBitmap)

            photoView.isZoomable = true

            recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
                setHasFixedSize(true)
                layoutManager = viewManager
                adapter = viewAdapter
            }
        }
        catch (e : Exception){

            Toast.makeText(this,
                "No Images in the folder /storage/emulated/0/DCIM/Camera/vaishakh/vaishakh/images/",
            Toast.LENGTH_LONG).show()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_photo_gallery)
        readImages()
    }









    inner class ScaleListener() : ScaleGestureDetector.SimpleOnScaleGestureListener() {

        var mScaleFactor = 1.0f
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            mScaleFactor *= scaleGestureDetector.scaleFactor
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f))
            imageView!!.setScaleX(mScaleFactor)
            imageView!!.setScaleY(mScaleFactor)
            return true
        }
    }





}
