package com.example.paycom.ui.main.fragments







import android.app.Fragment
import com.example.paycom.R


/**
 * A simple [Fragment] subclass.
 */
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


/**
 * A simple [Fragment] subclass.
 */
class ThirdFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_first, container, false)
    }
}