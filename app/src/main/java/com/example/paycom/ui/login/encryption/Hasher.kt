package com.example.paycom.ui.login.encryption

import java.security.MessageDigest

class Hasher {
   public fun hash( hashString:String): String {
        val bytes = hashString.toByteArray()
        val md = MessageDigest.getInstance("SHA-256")
        val digest = md.digest(bytes)
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }
}