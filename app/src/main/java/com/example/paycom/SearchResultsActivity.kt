package com.example.paycom

import ApiEndpoints.Event
import Database.EventDao
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.room.Room
import com.google.gson.Gson

class SearchResultsActivity : AppCompatActivity() {

    var eventDao : EventDao? = null

    var gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)
        val db = Room.databaseBuilder(
            applicationContext,
            Database.AppDatabase::class.java, "EventEntity"
        ).allowMainThreadQueries().build()

        eventDao = db.EventDao()


    }

    public fun callNewActivity(intent: Intent,event: Event){
        val eventString =  gson.toJson(event)
        intent.putExtra("event",eventString)

        startActivity(intent)
    }



    override fun onResume() {
        super.onResume()

        val queryString  =  intent.getStringExtra("queryString")
        var eventListfromDb= eventDao!!.getAll()

        var l3 =  findViewById<LinearLayout>(R.id.linear)

        l3.removeAllViewsInLayout()

        var arr : List<String> =  queryString!!.split("\\s+".toRegex())


        for(i in 0..(eventListfromDb.size -1)) {
            for (j in arr) {
                if (eventListfromDb.get(i).event!!.contains(j!!, ignoreCase = true)) {
                    var button = Button(this)
                    button.setText(eventListfromDb.get(i).eventno)
                    l3.addView(button)

                    button.setOnClickListener {

                        Toast.makeText(this, "hi", Toast.LENGTH_LONG).show()
                        val intent = Intent(this, DispalyEventDetailsActivity::class.java)
                        val currentEventFromDb =
                            gson.fromJson<Event>(eventListfromDb.get(i).event, Event::class.java)
                        callNewActivity(intent, currentEventFromDb)
                    }
                }
            }
        }
    }
}

