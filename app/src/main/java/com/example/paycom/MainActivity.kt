package com.example.paycom

import ApiEndpoints.Event
import ApiEndpoints.FetchEventType
import Backup.Parse
import Database.Config
import Database.EventDao
import admin.ViewModelStoreContacts
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.gson.Gson
import com.parse.ParseInstallation

import map.BackGroundLocationService



class MainActivity : AppCompatActivity() {

    val listOfPermissions = arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR,
        Manifest.permission.INTERNET,Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.READ_CONTACTS,Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION)

    var gson = Gson()

    var eventDao : EventDao? = null

    private fun makeRequest() {
        for(permission in listOfPermissions) {
            val permissionNeeded = ContextCompat.checkSelfPermission(
                this,
                permission
            )
            if (permissionNeeded != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    listOfPermissions,
                    101
                )
            }
        }
    }


    fun addToggleButton(){

        val toggle : ToggleButton = findViewById(R.id.toggleButton2)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // The toggle is enabled
                startService(Intent(this@MainActivity, BackGroundLocationService::class.java))
            } else {
                // The toggle is disabled
                stopService(Intent(this@MainActivity, BackGroundLocationService::class.java))
            }
        }


    }

    public fun callNewActivity(intent: Intent,event: Event){
        val gson = Gson()
       val eventString =  gson.toJson(event)
       intent.putExtra("event",eventString)
        startActivity(intent)
    }

    fun findAllSearchIds(query : String){
        if(query.length >0){
            var intent = Intent(this,SearchResultsActivity::class.java)
            var modifiedquery = query.trim()
            intent.putExtra("queryString",modifiedquery)
            startActivity(intent)
        }
    }

    fun addSearch(){
        var l1=findViewById(R.id.linear) as (LinearLayout);
        var searchBar = SearchView(this)

        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
            override fun onQueryTextSubmit(query: String): Boolean {
                // task HERE
                findAllSearchIds(query)
                return true }
        })
        l1.addView(searchBar)
    }

    fun addAdminButton(){
        val isAdmin = intent.getBooleanExtra("isAdmin",false)
        if(isAdmin){
            var l1=findViewById(R.id.linear) as (LinearLayout);
            var b =  Button(this);
            b.setText("Admin console");
            b.setOnClickListener { this
                val intent = Intent(this,AdminActivity::class.java)
                startActivity(intent)
            }
            l1.addView(b)
        }
    }

    fun addUploadButton(eventDao :EventDao){
        var l1=findViewById(R.id.linear) as (LinearLayout);
        var b =  Button(this);
        b.setText("Backup USER DATA LOOO");
        b.setOnClickListener { this
            var backupUserData = Parse(this)
          //  backupUserData.connectToParse()
            backupUserData.uploadAll(eventDao)
        }
        l1.addView(b)
    }

    fun addMapButton(){
        var b = Button(this)
        b.setText("Go to Maps This is POC for developers")
        val linearLayout = findViewById<LinearLayout>(R.id.linear)
        b.setOnClickListener {
            val intent = Intent(this,MapsActivity::class.java)
            startActivity(intent)
        }
        linearLayout.addView(b)
    }

    fun addEventMapButton(){
        var b = Button(this)
        b.setText("Maps TO get Location Details of Attendess")
        val linearLayout = findViewById<LinearLayout>(R.id.linear)
        b.setOnClickListener {
            val intent = Intent(this,MapEventLoadActivity::class.java)
            startActivity(intent)
        }
        linearLayout.addView(b)
    }

    fun addPhotoGalleryButton(){
        var b = Button(this)
        b.setText("Go to Photo gallery")
        val linearLayout = findViewById<LinearLayout>(R.id.linear)
        b.setOnClickListener {
            val intent = Intent(this,EventPhotoGalleryActivity::class.java)
            startActivity(intent)
        }
        linearLayout.addView(b)

    }


    override fun onCreate(savedInstanceState: Bundle?) {


            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            //Store it to a
           //

        makeRequest()


        val db = Room.databaseBuilder(
                applicationContext,
                Database.AppDatabase::class.java, "EventEntity"
            ).allowMainThreadQueries().build()
            eventDao = db.EventDao()

            addSearch()
            var fetchEventType = FetchEventType(this, applicationContext)

            fetchEventType.FetchJson()
            fetchEventType.addEventButtonToEventsInDb()
            addAdminButton()

            addMapButton()

        addEventMapButton()

        addPhotoGalleryButton()


            // backupUserData.getQuery(
            // )
            val config = Config()

            com.parse.Parse.initialize(
                com.parse.Parse.Configuration.Builder(this)
                    .applicationId("Mr42bvuYLKepnLgLH7tmDbHWOy92SNRkvLqwVsvN")
                    // if defined
                    .clientKey(
                        "GaGOsPemJvW88s0OFHVe0EXDapJ5Jo2DZylD5pj1"
                    )
                    .server("https://parseapi.back4app.com/")
                    .build()
            )
        //Hard coded this, because reading from config file is throwing errors.

            ParseInstallation.getCurrentInstallation().saveInBackground();

            addToggleButton()

        var backupUserData = Parse(this)
            backupUserData.fetchFromParseDbEventsNotPresentInLocalDbAndPopulateDB(eventDao!!)

        addUploadButton(eventDao!!)


    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.d("TAG", "Permission has been denied by user")

                } else {

                    Log.i("TAG", "Permission has been granted by user")
                }
            }
        }
        }

}
