package com.example.paycom

import Database.SharedPreferences.SharedPreferenceManagerForContactDetails
import admin.ContactsFetch
import admin.ListOfSelectedContacts
import admin.ViewModelStoreContacts
import android.graphics.Color
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_admin.*


import android.widget.TextView

import com.google.gson.Gson


class AdminActivity : AppCompatActivity() {

    val gson = Gson()


    fun saveSelectedButtonsToSharedPref(){
        var l2 = findViewById<LinearLayout>(R.id.linear)

        var ListOfContacts : MutableList<String> = mutableListOf<String>()

        for(i in 0..(l2.childCount -1))
        {
            val v =  l2!!.getChildAt(i) as View;
            if (v is Button) {
                val b = v
                if(b.tag==Color.RED)
                    ListOfContacts.add(b.text.toString())
            }
        }
        var listOfSelectedContacts = ListOfSelectedContacts(ListOfContacts)
        val listOfSelectedContactsString = gson.toJson(listOfSelectedContacts)
        val sharedPreferenceManagerForContactDetails = SharedPreferenceManagerForContactDetails(this)
        sharedPreferenceManagerForContactDetails.add(listOfSelectedContactsString)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
       // setSupportActionBar(toolbar)
        var l2 = findViewById<LinearLayout>(R.id.linear)
        var tv = TextView(this)
        tv.setText("Please wait till all the contacts Load. ADmin must wait!!!")
        l2.addView(tv)
        var fetchContacts = ViewModelStoreContacts(this)
        var viewModel = ContactsFetch(this)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Contact list has been updated and successfully added", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

            saveSelectedButtonsToSharedPref()
        }
    }

}
