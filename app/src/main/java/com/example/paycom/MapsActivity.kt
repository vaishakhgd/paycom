package com.example.paycom

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager




import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

    var mFusedLocationProviderClient : FusedLocationProviderClient? =null


    var googleMap : GoogleMap? =null

    val DEFAULT_ZOOM = 15f

    val listOfPermissions = arrayOf(
         Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION)


    val minTimeForRequesTingLocation = 1000

   val  minDistanceForRequesTingLocation = 0.toFloat()

    private lateinit var locationCallback: LocationCallback




    private fun startLocationUpdates() {

        val locationRequest = LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

          locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    // Update UI with location data
                    // ...

                    val latLng = LatLng(location!!.latitude,location!!.longitude)

                    googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM))
                    if (title != "My Location") {
                        val options = MarkerOptions()
                            .position(latLng)
                            .title("LocationCallBack")
                        googleMap!!.addMarker(options)
                    }
                }
            }
        }


        mFusedLocationProviderClient!!.requestLocationUpdates(locationRequest,
            locationCallback,
            Looper.getMainLooper())
    }



    override fun onCreate(savedInstanceState: Bundle?) {

        makeRequest()

        try {
            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_maps)
            val mapFragment: SupportMapFragment? =
                supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
            mapFragment?.getMapAsync(this)

            startLocationUpdates()

        }
        catch (e : Exception){

        }



    }



    private fun moveCamera(
        latLng: LatLng,
        zoom: Float,
        title: String
    ) {
        Log.d(
           "Yo",
            "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude
        )

        hideSoftKeyboard()
    }

    private fun hideSoftKeyboard() {
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }


    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }





        override fun onMapReady(googleMapOnReady: GoogleMap?) {


            googleMap = googleMapOnReady

            mFusedLocationProviderClient!!.lastLocation
                .addOnSuccessListener { location : Location? ->
                    // Got last known location. In some rare situations this can be null.

                    try{
                        val latLng = LatLng(location!!.latitude,location!!.longitude)

                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM))
                        if (title != "My Location") {
                            val options = MarkerOptions()
                                .position(latLng)
                                .title("Hi Bro")
                            googleMap!!.addMarker(options)
                        }

                    }

                    catch (e : java.lang.Exception){}


                }



    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private fun makeRequest() {
        for(permission in listOfPermissions) {
            val permissionNeeded = ContextCompat.checkSelfPermission(
                this,
                permission
            )
            if (permissionNeeded != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    listOfPermissions,
                    101)
            }
          else  if(permissionNeeded == PackageManager.PERMISSION_GRANTED){

                Toast.makeText(application,"Permission Package manager granted",Toast.LENGTH_LONG).show()


            }
        }
    }

    override fun onLocationChanged(p0: Location?) {
        TODO("Not yet implemented")
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.d("TAG", "Permission has been denied by user")

                } else {
                    makeRequest()
                    Log.i("TAG", "Permission has been granted by user")
                    val locationManager =getSystemService(Context.LOCATION_SERVICE) as (LocationManager)

                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    }
                }
            }
        }
    }

}

