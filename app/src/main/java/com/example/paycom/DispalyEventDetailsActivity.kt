package com.example.paycom

import ApiEndpoints.Event
import Database.Config
import Database.EventDao
import Database.EventEntity
import admin.AddAdminUI
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import calendar.WriteToCalendar
import com.google.gson.Gson
import com.sendbird.android.SendBird
import map.BackGroundLocationService


class DispalyEventDetailsActivity : AppCompatActivity() {

    var isInternetAvailable = false

    var eventGlobal : Event? = null

    val gson = Gson()


    public fun getEvent(eventDao : EventDao) : Event{
        val eventStringFromIntent=intent.getStringExtra("event")
        val gson = Gson()
        var eventFromIntent = gson.fromJson<Event>(eventStringFromIntent,Event::class.java)
        val daoList = eventDao.loadAllByEventNo(eventFromIntent.eventno)
        if(daoList.size>0){
            eventFromIntent =  gson.fromJson<Event>(daoList.get(0).event,Event::class.java)
        }
        return eventFromIntent
    }

    public fun addSessionButton(eventFromIntent : Event){
        var linearLayout = findViewById(R.id.linear) as LinearLayout
        for(i in 0..(eventGlobal!!.isGoing.size)-1 ){
            var b = Button(this)
            if(eventGlobal!!.isGoing.get(i) == true){
                b.setBackgroundColor(Color.RED)
                b.setTextColor(Color.WHITE)
                b.tag =Color.RED
            }
            if(eventGlobal!!.isGoing.get(i)==false){
                b.setBackgroundColor(Color.WHITE)
                b.setTextColor(Color.BLACK)
                b.tag = Color.WHITE
            }
            b.setText(eventFromIntent.sessionList.get(i))
            b.setOnClickListener {
                var currentButtonIndex =0
                for(i in 0..(eventGlobal!!.sessionList.size -1)) {
                    if (b.text == eventGlobal!!.sessionList.get(i)) {
                        currentButtonIndex = i
                    }
                }
                if( b.tag == Color.WHITE){
                    b.setBackgroundColor(Color.RED)
                    b.setTextColor(Color.WHITE)
                    b.setTag(Color.RED)
                    eventGlobal?.isGoing?.set(currentButtonIndex,true)
                }
                else if (b.tag == Color.RED){
                    b.setBackgroundColor(Color.WHITE)
                    b.setTextColor(Color.BLACK)
                    b.setTag(Color.WHITE)
                    eventGlobal?.isGoing?.set(currentButtonIndex,false)
                }
            }
            linearLayout.addView(b)
        }
    }

    fun appendEventDetails(eventFromIntent : Event){
        var linearLayout = findViewById(R.id.linear) as LinearLayout
        var eString = "eventid " + eventFromIntent.eventno+ " event title "+eventFromIntent.title+ " event description "+ eventFromIntent.description

        var tv = TextView(this)
        linearLayout.addView(tv)
        eString+= " Guest list "

        for(i in eventGlobal!!.guestlist){
            eString+= i
        }
        tv.setSingleLine(false);
        tv.setLines(10)
        tv.setEllipsize(TextUtils.TruncateAt.END);

        tv.setText(eString)

    }







    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispaly_event_details)

        stopService(Intent(this, BackGroundLocationService::class.java))

        val db = Room.databaseBuilder(
            applicationContext,
            Database.AppDatabase::class.java, "EventEntity"
        ).allowMainThreadQueries().build()

        var eventDao = db.EventDao()
        val conf = Config()
       var c =  conf.getFromConfig(this,"Sendbird.send_bird_app_id")

        SendBird.init(c , this);
        SendBird.connect("Imvaish", SendBird.ConnectHandler { user, e ->
            if (e != null) {    // Error.
                return@ConnectHandler
            }
            else{
                isInternetAvailable = true
                val intent = Intent(this, ChatWindowActivity::class.java)
                //startActivity(intent)
                }
        })


        eventGlobal = getEvent(eventDao)
        val eventFromIntent = getEvent(eventDao)
        var linearLayout = findViewById(R.id.linear) as LinearLayout

        appendEventDetails(eventFromIntent)
        addSessionButton(eventFromIntent)

        var eventGlobalStr = gson.toJson(eventGlobal)
        var b = Button(this)
        b.setText("GO to chat page")
        linearLayout.addView(b)

        b.setOnClickListener {
            SendBird.connect("Imvaish", SendBird.ConnectHandler { user, e ->
                if (e != null) {    // Error.
                    isInternetAvailable= false
                    return@ConnectHandler
                } else{
                    isInternetAvailable = true
                    val intent = Intent(this, ChatWindowActivity::class.java)
                    intent.putExtra("channelUrlIdeallyMustBeEqualToEventId",eventFromIntent.eventno)
                    startActivity(intent)
                }
            })
        }

        var  updateEventButton = Button(this)
        updateEventButton.setText("Update event Info")
        updateEventButton.setOnClickListener {
            var eventEntity = EventEntity(eventGlobal!!.eventno,gson.toJson(eventGlobal),System.currentTimeMillis())

            eventDao.deleteAllUsersOfEventno(eventEntity.eventno)
            eventDao.insertAll(eventEntity)
        }
        updateEventButton.setText("Update event Info")
        linearLayout.addView(updateEventButton)


        var  saveToLocalCalendarButton = Button(this)
        saveToLocalCalendarButton.setText("Save Event to Local calendar")
        var writeToCalendar = WriteToCalendar(this)
        saveToLocalCalendarButton.setOnClickListener{
            var eventEntity = EventEntity(eventGlobal!!.eventno,gson.toJson(eventGlobal),System.currentTimeMillis())
            writeToCalendar.findIfEventPresentInCalendar(eventEntity)
        }


        linearLayout.addView(saveToLocalCalendarButton)

        val addAdminUI = AddAdminUI(this)
        addAdminUI.addContactsSelectedList(eventGlobal)


    }


}
