package Database

import android.content.Context
import android.util.Log
import com.example.paycom.R
import java.lang.Exception
import java.util.*

import java.io.IOException


class Config {
    private val TAG = "Helper"

   public  fun getFromConfig(ctx : Context, name : String) : String {

       val resources = ctx.getResources();



       try {
           val rawResource = resources.openRawResource(R.raw.config);
           val properties =  Properties();
           properties.load(rawResource);
           return properties.getProperty(name);
       } catch ( e : Exception) {
           Log.e(TAG, "Unable to find the config file: " + e.message);
       } catch (e: IOException) {
           Log.e(TAG, "Failed to open config file.");
       }

       return "";


   }
}
