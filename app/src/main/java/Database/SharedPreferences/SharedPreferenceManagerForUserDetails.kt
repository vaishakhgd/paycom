package Database.SharedPreferences

import Database.Config
import android.content.Context
import com.example.paycom.data.model.UserDetails
import com.example.paycom.ui.login.encryption.Hasher
import com.google.gson.Gson


class SharedPreferenceManagerForUserDetails(ctx2 :Context) {

   val ctx:  Context

    val gson = Gson()

    init{
        ctx = ctx2
    }
    public  fun getSharedPreferenceString(sharedPref  :String, key:String) : String{
        val sharedPrefObject = ctx.applicationContext.getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        val config = Config()
        return sharedPrefObject.getString(key,config.getFromConfig(ctx,"SharedPreferences.defaultString"))!!
    }



    public  fun isUserDetailsStored() : Boolean{
        val config = Config()
        val sharedPrefObject = ctx.applicationContext.getSharedPreferences(config.getFromConfig(ctx,"SharedPreferences.sharedpreferencenameForUserDetails"), Context.MODE_PRIVATE);
        return sharedPrefObject.getBoolean(config.getFromConfig(ctx,"SharedPreferences.isUserDetailsStoredKey"),false)
    }

    public  fun storeUserDetails(userDetails : UserDetails){
        val config = Config()
        val sharedPrefObject = ctx.applicationContext.getSharedPreferences(config.getFromConfig(ctx,"SharedPreferences.sharedpreferencenameForUserDetails"), Context.MODE_PRIVATE);
        val hasher = Hasher()
        userDetails.password = hasher.hash(userDetails.password)

        val userDetailsString = gson.toJson(userDetails)
        with (sharedPrefObject.edit()) {
            putString(config.getFromConfig(ctx,"SharedPreferences.UserDetailskey"), userDetailsString)
            putBoolean(config.getFromConfig(ctx,"SharedPreferences.isUserDetailsStoredKey"),true)
            commit()
        }
    }

    public  fun getSavedUserDetails(): UserDetails{

        val config = Config()
        val sharedPrefObject = ctx.applicationContext.getSharedPreferences(config.getFromConfig(ctx,"SharedPreferences.sharedpreferencenameForUserDetails"), Context.MODE_PRIVATE);
        val str = sharedPrefObject.getString(config.getFromConfig(ctx,"SharedPreferences.UserDetailskey"),config.getFromConfig(ctx,"SharedPreferences.DefaultUserDetailsvalue"))
        val userDetails = gson.fromJson<UserDetails>(str,UserDetails::class.java)

        return userDetails
    }





}