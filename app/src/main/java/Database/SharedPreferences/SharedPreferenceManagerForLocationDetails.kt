package Database.SharedPreferences

import Database.Config
import android.content.Context
import android.location.Location
import android.widget.Toast
import com.example.paycom.data.model.UserDetails
import com.example.paycom.ui.login.encryption.Hasher
import com.google.gson.Gson
import map.LocationData
import map.StoreDataToParse.ListOfLocationData

class SharedPreferenceManagerForLocationDetails(ctx : Context) {
    val ctx2: Context
    init{
        ctx2 = ctx
    }

    val gson = Gson()

    public  fun storeLocationDetails(location : LocationData){


        val sharedPrefObject = ctx2.getSharedPreferences("locationData", Context.MODE_PRIVATE);

        val locationString = gson.toJson(location)
        with (sharedPrefObject.edit()) {
            putString("locationDetails", locationString)
            putBoolean("isLocationStored",true)
            commit()
        }
    }

    public  fun getSavedLocationDetails(): LocationData {

        val config = Config()
        val sharedPrefObject = ctx2.getSharedPreferences("locationData", Context.MODE_PRIVATE);
       val  str = sharedPrefObject.getString("locationDetails", "noDetails")

        val locationDetails = gson.fromJson<LocationData>(str, LocationData::class.java)

        return locationDetails
    }

    fun isLocationStored(): Boolean{
        val sharedPrefObject = ctx2.getSharedPreferences("locationData", Context.MODE_PRIVATE);

        val isLocationStored = sharedPrefObject.getBoolean("isLocationStored",false)

        return isLocationStored
    }

}