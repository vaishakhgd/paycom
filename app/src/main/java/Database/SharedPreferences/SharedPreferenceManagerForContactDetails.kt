package Database.SharedPreferences

import admin.ListOfSelectedContacts
import android.content.Context
import com.google.gson.Gson

class SharedPreferenceManagerForContactDetails(ctx : Context) {

   val ctx2: Context



    init{
        ctx2 = ctx

    }

    fun add(value : String){
        //value must be json.
        val sharedPrefObject = ctx2.applicationContext.getSharedPreferences(
            "SharedPreferenceManagerForContactDetails", Context.MODE_PRIVATE);

        val key = "SharedPreferenceManagerKey"

        with (sharedPrefObject.edit()) {
            putString(key, value)
            commit()
        }
    }

    fun get() : ListOfSelectedContacts {
        val key = "SharedPreferenceManagerKey"
        val sharedPrefObject = ctx2.applicationContext.getSharedPreferences(
            "SharedPreferenceManagerForContactDetails", Context.MODE_PRIVATE);
        val listContacts = sharedPrefObject.getString(key, """ { listOfSelectedContacts : ["No contact added"]}""")
        val gson = Gson()

       val listOfSelectedContacts =  gson.fromJson<ListOfSelectedContacts>(listContacts,ListOfSelectedContacts::class.java)
       return listOfSelectedContacts
    }
}