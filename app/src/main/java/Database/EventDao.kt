package Database

import androidx.room.*

@Dao
interface EventDao {




    @Query("SELECT * FROM EventEntity")
    fun getAll(): List<EventEntity>

    @Query("SELECT * FROM EventEntity WHERE eventno =:eventno order by id desc")
    fun loadAllByEventNo(eventno: String?): List<EventEntity>

    @Query("Delete FROM EventEntity WHERE eventno =:eventno ")
    fun deleteAllUsersOfEventno(eventno: String?)

    @Query("Select *  FROM EventEntity where id in (SELECT MIN(id) FROM EventEntity GROUP BY eventno)")
    fun getAllDistinct() : List<EventEntity>



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg users: EventEntity?)

    @Delete()
    fun delete(user: EventEntity)
}

