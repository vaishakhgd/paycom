package Database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters


@Entity(tableName = "EventEntity")
data class EventEntity(


    @ColumnInfo(name = "eventno") var eventno: String,
    @ColumnInfo(name = "event") var event: String?,
    @ColumnInfo(name = "timestamp") var timestamp: Long?



    /* @ColumnInfo(name = "guestList") var guestList: String?,

     @ColumnInfo(name = "sessionList") var sessionList: String?,

     @ColumnInfo(name = "isGoing") var isGoing: String?
*/

)
{
    @PrimaryKey(autoGenerate = true) var id: Int =0
}

/*
*
*
*
*
* class Event(val eventno: String, val description: String, var title: String, var time : String ,var Date : String,
            var guestlist : MutableList<String>,var sessionList : MutableList<String> ,var isGoing : MutableList<Boolean>  )
* */