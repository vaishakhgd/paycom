package Backup

import ApiEndpoints.Event
import Database.Config
import Database.EventDao
import Database.EventEntity
import Database.SharedPreferences.SharedPreferenceManagerForUserDetails
import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.example.paycom.DispalyEventDetailsActivity
import com.example.paycom.MainActivity
import com.example.paycom.R
import com.google.gson.Gson
import com.parse.Parse
import com.parse.ParseInstallation
import com.parse.ParseObject
import com.parse.ParseQuery

class Parse(ctx : MainActivity) {

    val ctx2: MainActivity

    val config = Config()

    var listParseObject: List<ParseObject>? = null

    var isParseObjectReturned = false

    init {
        ctx2 = ctx
    }

    public fun addEventButtonsOfDb(eventEntity: EventEntity){

        val gson = Gson()
        var l3 =  ctx2.findViewById<LinearLayout>(R.id.linear)
        var button = Button(ctx2)
        button.setText(eventEntity.eventno)
        l3.addView(button)
        button.setOnClickListener {

            Toast.makeText(ctx2, "hi", Toast.LENGTH_LONG).show()

            val intent = Intent(ctx2, DispalyEventDetailsActivity::class.java)

            val currentEventFromDb =
                gson.fromJson<Event>(eventEntity.event, Event::class.java)

            ctx2.callNewActivity(intent, currentEventFromDb)
        }


    }



    public  fun fetchFromParseDbEventsNotPresentInLocalDbAndPopulateDB(eventDao :EventDao){

        val query = ParseQuery.getQuery<ParseObject>("PaycomUserData")
        val sharedPreferenceManagerForUserDetails = SharedPreferenceManagerForUserDetails(ctx2)

        val userDetails = sharedPreferenceManagerForUserDetails.getSavedUserDetails()

        Toast.makeText(ctx2,userDetails.email,Toast.LENGTH_LONG ).show()

        query.whereEqualTo("emailid",userDetails.email)

        query.findInBackground {objects, e ->
            if (e == null) {
                for (parseObject in objects) {
                    val listWithEventIds = eventDao.loadAllByEventNo(parseObject.getString("eventId"))
                    if(listWithEventIds.size==0){
                        val eventEntity = EventEntity(parseObject.getString("eventId")!!,parseObject.getString("eventJson"),System.currentTimeMillis())
                        eventDao.insertAll(eventEntity)
                        Toast.makeText(ctx2,parseObject.getString("eventJson").toString(),Toast.LENGTH_LONG ).show()
                        Log.i("listWithEventIds.size==0",   parseObject.getString("eventJson").toString() )
                        addEventButtonsOfDb(eventEntity)
                    }
                }
            } else {
            }
        }
    }



    public fun connectToParse() {

        var applicationId = config.getFromConfig(ctx2, "Parse.applicationId")
        var clientKey = config.getFromConfig(ctx2, "Parse.clientKey")
        var serverUrl = config.getFromConfig(ctx2, "Parse.serverUrl")
        Parse.initialize( Parse.Configuration.Builder(ctx2.applicationContext)
            .applicationId("Mr42bvuYLKepnLgLH7tmDbHWOy92SNRkvLqwVsvN")
            // if defined
            .clientKey("GaGOsPemJvW88s0OFHVe0EXDapJ5Jo2DZylD5pj1")
            .server("https://parseapi.back4app.com/")
            .build()
        );

        ParseInstallation.getCurrentInstallation().saveInBackground();


    }


    public fun deleteExisting(eventDao: EventDao){

       val query = ParseQuery.getQuery<ParseObject>("PaycomUserData")
       val sharedPreferenceManagerForUserDetails = SharedPreferenceManagerForUserDetails(ctx2)

       val userDetails = sharedPreferenceManagerForUserDetails.getSavedUserDetails()

       query.whereEqualTo("emailid",userDetails.email.toString())

       query.findInBackground {objects, e ->
           if (e == null) {
               for (parseObject in objects) {
                   parseObject.delete()//calling blocking system call because, only upon successful deletion upload must happen
                   //deleteInBackGround() is the non blocking call. As,Parse doesnt have direct override existing data, feature, we must delete anf upload
               }
              uponDeletionUpload(eventDao)
           } else {
           }
       }

    }

fun uponDeletionUpload(eventDao: EventDao) {

    val entityList = eventDao.getAll()
    Log.i("LOOOOOOOO","SIZE OFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF "+entityList.size)
    val sharedPreferenceManagerForUserDetails = SharedPreferenceManagerForUserDetails(ctx2)
    val userDetails = sharedPreferenceManagerForUserDetails.getSavedUserDetails()

    val uploadParseObject = ParseObject("PaycomUserData")

    for (i in 0..(entityList.size - 1)) {
        val uploadParseObject = ParseObject("PaycomUserData")
        Log.i("LOOOOOOOO","SEDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE "+entityList.get(i).eventno)
        uploadParseObject.put("emailid", userDetails.email);
        uploadParseObject.put("timestamp", entityList.get(i).timestamp!!);
        uploadParseObject.put("eventId", entityList.get(i).eventno);
        uploadParseObject.put("eventJson", entityList.get(i).event!!);
        Toast.makeText(ctx2.applicationContext, "Now uploading Data", Toast.LENGTH_LONG).show()
        uploadParseObject.saveInBackground()
    }
}

public fun uploadAll(eventDao: EventDao) {
    deleteExisting(eventDao)
    }
}