package ApiEndpoints

import Database.EventEntity
import android.content.Context
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.example.paycom.MainActivity
import com.example.paycom.R
import com.google.gson.*

import android.content.Intent

import com.example.paycom.DispalyEventDetailsActivity
import java.lang.Exception


class FetchEventType ( ctx :MainActivity, applicationContext : Context ) {

    val ctx2: MainActivity

    val applicationContext2 :Context

    init {
        ctx2=ctx
        applicationContext2 =applicationContext
    }

    //Get
    public  fun FetchFromApi():String?{

        return """{"eventno" : "event1225" ,"description": "This is description", "title" : "this is title tagaru shiva" , "time" : "2PM-4PM", "Date" : "21-08-2020","guestlist" : ["David","Mike","George"],"sessionList" : ["11-12","12-13"],"isGoing" : [false,false], "eventId" : 1225 }"""
    }

    //Get This
    public fun FetchFromApi2():String?{

        return """{"eventno" : "event1236" ,"description": "This is description", "title" : "this is title appu" , "time" : "2PM-4PM", "Date" : "22-08-2020","guestlist" : ["David","Mike","George"],"sessionList" : ["14-15","15-16","16-17","17-18"],"isGoing" : [false,false,false,false] , "eventId" : 1236}"""
    }


    fun addEventButtonToEventsInDb(){
        var mainActivity : MainActivity

        mainActivity = ctx2

        var l3 =  ctx2.findViewById<LinearLayout>(R.id.linear)



        var gson = Gson()

        var eAllENtity =  mainActivity.eventDao!!.getAll()
        for (i in 0..(eAllENtity.size - 1)) {


            Log.i ("Yo","XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx "
                    +eAllENtity.get(i).event)

            var button = Button(ctx2)
            button.setText(eAllENtity.get(i).eventno)
            l3.addView(button)

            button.setOnClickListener {

                Toast.makeText(ctx2, "hi", Toast.LENGTH_LONG).show()

                val intent = Intent(ctx2, DispalyEventDetailsActivity::class.java)

                val currentEventFromDb =
                    gson.fromJson<Event>(eAllENtity.get(i).event, Event::class.java)

                ctx2.callNewActivity(intent, currentEventFromDb)
            }



        }

    }

    public fun populateFieldsOnMain(event : Event){
        var mainActivity : MainActivity

        mainActivity = ctx2

        var l3 =  ctx2.findViewById<LinearLayout>(R.id.linear)

        var gson = Gson()

        var eAllENtity =  mainActivity.eventDao!!.getAll()

        var isEventAlreadyPresentInDb = false

        for( i in 0..(eAllENtity.size -1)){
            if(eAllENtity.get(i).eventno == event.eventno){
                isEventAlreadyPresentInDb = true
            }
        }

        if(isEventAlreadyPresentInDb ==false){
            Log.i("YUPPPPPPPPPPPPP","YUPPPPPPPPPPPPPPPPPPPPPPPPPPPppp isEventAlreadyPresentInDb = true" )
            var eveString = gson.toJson(event)
            val eEntity = EventEntity(event.eventno,eveString,System.currentTimeMillis())
            mainActivity.eventDao!!.insertAll(eEntity)

            var button = Button(ctx2)
            button.setText(event.eventno)
            l3.addView(button)

            button.setOnClickListener {

                Toast.makeText(ctx2, "hi", Toast.LENGTH_LONG).show()

                val intent = Intent(ctx2, DispalyEventDetailsActivity::class.java)

                ctx2.callNewActivity(intent, event)
            }

        }
    }

    //Push Notification FROM API calls this method
    public fun FetchJson(){

        try {
            var jsonString = FetchFromApi()
            var gson = Gson()
            var x = gson.fromJson<Event>(jsonString, Event::class.java)
            populateFieldsOnMain(x)

            jsonString = FetchFromApi2()

            x = gson.fromJson<Event>(jsonString, Event::class.java)
            populateFieldsOnMain(x)
        }
        catch (e : Exception){
            Toast.makeText(ctx2, "Unable to fetch from JSon", Toast.LENGTH_LONG).show()
        }
    }




}