package admin

import ApiEndpoints.Event
import ApiEndpoints.SendToServer
import Database.SharedPreferences.SharedPreferenceManagerForContactDetails
import Database.SharedPreferences.SharedPreferenceManagerForUserDetails
import android.graphics.Color
import android.view.View
import android.widget.*
import com.example.paycom.DispalyEventDetailsActivity
import com.example.paycom.R
import com.google.gson.Gson

class AddAdminUI(ctx2 : DispalyEventDetailsActivity) {

    val ctx : DispalyEventDetailsActivity
    init {
        ctx = ctx2 }

    fun onSubmitUploadEvent() {

        try {
            var linearLayout = ctx.findViewById(R.id.linear) as LinearLayout
            val sharedPreferenceManagerForUserDetails = SharedPreferenceManagerForUserDetails(ctx)
            val isAdmin =
                sharedPreferenceManagerForUserDetails.getSavedUserDetails().email.toLowerCase()
                    .contains("admin")
            if (isAdmin) {
                var title = ""
                var eventno = ""
                var description = ""
                var Date = ""
                var time = ""
                var guestlist: MutableList<String> = mutableListOf<String>()
                var sessionList: MutableList<String> = mutableListOf<String>()
                var eventNo: Long = 8

                var isGoing: MutableList<Boolean> = mutableListOf<Boolean>()
                for (i in 0..(linearLayout.childCount - 1)) {
                    val v = linearLayout!!.getChildAt(i) as View;
                    if (v.tag == "title") {
                        var titleTag = v as (EditText)
                        title = titleTag.text.toString()
                    }
                    if (v.tag == "eventId") {
                        var eventTag = v as (EditText)
                        eventno = eventTag.text.toString()
                    }
                    if (v.tag == "description") {
                        var descriptionTag = v as (EditText)
                        description = descriptionTag.text.toString()
                    }
                    if (v.tag == "sessionList") {
                        var sessionTag = v as (EditText)
                        sessionList = sessionTag.text.toString().split(",").toMutableList()
                    }
                    if (v.tag == "time") {
                        var timeTag = v as EditText
                        time = timeTag.text.toString()
                    }
                    if (v.tag == "date") {
                        var dateTag = v as EditText
                        Date = dateTag.text.toString()
                    }
                    if (v is Button) {
                        var b = v as Button
                        if (b.tag == Color.BLUE) {
                            guestlist.add(b.text.toString())
                        }
                    }
                }
                for (session in sessionList) {
                    isGoing.add(false)
                }
                val event = Event(
                    eventno = eventno,
                    eventId = eventNo,
                    isGoing = isGoing,
                    Date = Date,
                    time = time,
                    guestlist = guestlist,
                    sessionList = sessionList,
                    title = title,
                    description = description
                )
                val gson = Gson()
                val eventStr = gson.toJson(event)
                Toast.makeText(ctx.applicationContext, eventStr, Toast.LENGTH_LONG).show()
                val sendToServer = SendToServer()
                sendToServer.upLoad(eventStr)
            }

        }
        catch (e : Exception){Toast.makeText(ctx , "Error uploading ",Toast.LENGTH_LONG).show()}
    }

    fun addContactsSelectedList(eventGlobal : Event?){


        var linearLayout = ctx.findViewById(R.id.linear) as LinearLayout

        val sharedPreferenceManagerForUserDetails = SharedPreferenceManagerForUserDetails(ctx)

        val isAdmin = sharedPreferenceManagerForUserDetails.getSavedUserDetails().email.toLowerCase().contains("admin")

        if(isAdmin) {

            val tv = TextView(ctx)
            tv.setText("ADMIN Console")
            linearLayout.addView(tv)

            val tv2 = TextView(ctx)
            tv2.setText("eventId")
            linearLayout.addView(tv2)

            val eventId = EditText(ctx)
            eventId.setText(eventGlobal!!.eventno)
            eventId.setTag("eventId")
            linearLayout.addView(eventId)

            val tv3 = TextView(ctx)
            tv3.setText("title")
            linearLayout.addView(tv3)

            val title = EditText(ctx)
            title.setText(eventGlobal!!.title)
            title.setTag("title")
            linearLayout.addView(title)

            val tv4 = TextView(ctx)
            tv4.setText("description")
            linearLayout.addView(tv4)

            val description = EditText(ctx)
            description.setText(eventGlobal!!.description)
            description.setTag("description")
            linearLayout.addView(description)

            val tv5 = TextView(ctx)
            tv5.setText("Date")
            linearLayout.addView(tv5)

            val date = EditText(ctx)
            date.setText(eventGlobal!!.Date)
            date.setTag("date")
            linearLayout.addView(date)

            val tv6 = TextView(ctx)
            tv6.setText("Time")
            linearLayout.addView(tv6)

            val time = EditText(ctx)
            time.setText(eventGlobal!!.time)
            time.setTag("time")
            linearLayout.addView(time)


            val tv7 = TextView(ctx)
            tv7.setText("SessionList")
            linearLayout.addView(tv7)

            val sessionList = EditText(ctx)
            sessionList.setText(eventGlobal!!.sessionList.joinToString( separator = ","))
            sessionList.setTag("sessionList")
            linearLayout.addView(sessionList)

            val sharedPreferenceManagerForContactDetails =
                SharedPreferenceManagerForContactDetails(ctx)
            val listOfSelectedContacts = sharedPreferenceManagerForContactDetails.get()
            val listOfSelectedContactsList = listOfSelectedContacts.listOfSelectedContacts

            for (i in 0..(listOfSelectedContactsList.size - 1)) {

                var b = Button(ctx)
                b.text = listOfSelectedContactsList.get(i)
                b.setBackgroundColor(Color.BLUE)
                b.setTextColor(Color.WHITE)
                b.setTag(Color.BLUE)

                b.setOnClickListener {
                    if(b.tag ==Color.BLUE){
                        b.setTag(Color.WHITE)
                        b.setBackgroundColor(Color.WHITE)
                        b.setTextColor(Color.BLACK)
                    }

                    else if(b.tag ==Color.WHITE){
                        b.setTag(Color.BLUE)
                        b.setBackgroundColor(Color.BLUE)
                        b.setTextColor(Color.WHITE)
                    }
                }
                linearLayout.addView(b)
            }
            var submit = Button(ctx)
            submit.setText("SUBmit")
            submit.setBackgroundColor(Color.YELLOW)
            submit.setTextColor(Color.BLACK)

            submit.setOnClickListener {
                onSubmitUploadEvent()
            }
            linearLayout.addView(submit)
        }
    }


}