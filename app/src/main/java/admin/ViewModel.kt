package admin

import android.content.Context
import android.graphics.Color
import android.provider.ContactsContract
import android.widget.Button
import android.widget.LinearLayout
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.loader.content.CursorLoader
import com.example.paycom.AdminActivity
import com.example.paycom.R
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class ContactsFetch(ctx : AdminActivity): ViewModel() {

    fun SelectContacts(b:Button){
        if( b.tag == Color.WHITE){
            //      Log.i("color is", "WHITEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
            b.setBackgroundColor(Color.RED)
            b.setTextColor(Color.WHITE)
            b.setTag(Color.RED)
        }
        else if (b.tag == Color.RED){
            //    Log.i("color is", "REDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD")
            b.setBackgroundColor(Color.WHITE)
            b.setTextColor(Color.BLACK)
            b.setTag(Color.WHITE)
        }

    }


    fun setInitialButtonConfigurations(b : Button){
        b.setBackgroundColor(Color.WHITE)
        b.setTextColor(Color.BLACK)
        b.setTag(Color.WHITE)
        b.setWidth(10);
        b.setHeight(100);
    }




    fun addContacts2(ctx : AdminActivity){
        var l2 = ctx.findViewById<LinearLayout>(R.id.linear)
        val PROJECTION = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        )

        val cursorLoader = CursorLoader(
            ctx,
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            PROJECTION,
            null,
            null,
            "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME
                    + ")ASC"
        )

        val c = cursorLoader.loadInBackground()

        if (c!!.moveToFirst()) {

            val Number = c
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val Name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)

            do {

                try {
                    val phNumber = c.getString(Number)
                    val phName = c.getString(Name)

                    var b = Button(ctx)
                    b.setText(phName + "" + phNumber)
                    setInitialButtonConfigurations(b)

                    b.setOnClickListener {
                        SelectContacts(b)
                    }
                    l2.addView(b)

                } catch (e: Exception) {

                }

            } while (c.moveToNext())

            c.close()
        }
    }

    fun addAlreadyObTainedContacts(ctx: AdminActivity){

        var l2 = ctx.findViewById<LinearLayout>(R.id.linear)

        val contactList = ListOfContactsStored.contactsList
        if(contactList.size > 0){
            for(i in 0..(contactList.size -1)){
                val contactString = contactList.get(i)
                var b = Button(ctx)
                b.setText(contactString)
                setInitialButtonConfigurations(b)

                b.setOnClickListener {
                    SelectContacts(b)
                }
                l2.addView(b)
            }

        }

    }



    init {
        viewModelScope.launch {
            // Coroutine that will be canceled when the ViewModel is cleared.
            val contactsListAsync = async { addAlreadyObTainedContacts(ctx) }
            val contacts = contactsListAsync.await()
        }
    }
}