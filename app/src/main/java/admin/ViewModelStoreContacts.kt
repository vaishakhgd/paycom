package admin

import android.content.Context
import android.provider.ContactsContract
import android.widget.Button
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.loader.content.CursorLoader
import com.example.paycom.AdminActivity
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ViewModelStoreContacts(ctx2 : Context): ViewModel() {

    val ctx : Context
    init {
        ctx = ctx2
        viewModelScope.launch {
            // Coroutine that will be canceled when the ViewModel is cleared.
            val contactsListAsync = async { addContacts() }
            val contacts = contactsListAsync.await()
        }
    }

    fun addContacts(){
        val PROJECTION = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        )

        val contacts = mutableListOf<String>()

        val cursorLoader = CursorLoader(
            ctx,
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            PROJECTION,
            null,
            null,
            "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME
                    + ")ASC"
        )

        val c = cursorLoader.loadInBackground()

        if (c!!.moveToFirst()) {

            val Number = c
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val Name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)

            do {

                try {
                    val phNumber = c.getString(Number)
                    val phName = c.getString(Name)
                    contacts.add(phName+"::"+phNumber)
                } catch (e: Exception) {

                }

            } while (c.moveToNext())

            c.close()
        }

        ListOfContactsStored.contactsList = contacts
    }

}